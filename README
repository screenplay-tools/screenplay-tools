1 Introduction
**************

screenplay-tools is a Slackermedia project that provides a powerful set of helper scripts to aid:

1. The screenwriter in formatting and printing a screenplay written in screenwriter-mode in emacs.

2. The Assistant Director or Producer in getting important scheduling-related reports from the screenplay.

I'll cover these in the most logical order.



2 Installation
**************

screenplay-tools requires no special apps aside from the typical GNU utils that are probably already on your system.  It does presume that you are using Emacs in screenwriter-mode, available from nongnu.org/screenwriter

To install into ~/bin, which is where I keep this stuff, simply cd into the screenplay-tools directory and run the install script.  A typical scenario would be:

   bash$ tar -xf screenplay-tools.tgz
   bash$ cd ~/screenplay-tools
   bash$ ./install.sh
   (enter your password)

This assumes you actually HAVE a ~/bin.
If not, you should do this first:

   bash$ mkdir ~/bin
   bash$ PATH=$PATH:/home/$USER/bin
   bash$ export PATH
 
THEN run the install script.

This does NOT require root privs because by default it installs everything into your home bin directory.  If you really want these cute lil' shell scripts available for all users of your system, just cp them, as root, to some place like /usr/local/bin/

     bash# cp ./screenplay-tools/* /usr/local/bin/


2.1 Man Pages and Other Forms of Help
=====================================

First time using screenplay-tools?  Have no idea how to use it?

Well, the file you are reading right now is a great intro to the toolchain, but you can also install man pages by running, as root, installman.sh

This installs man pages to /usr/share/man/man1/

You can also find an in-line quick-reference guide by running screenplay-help


3 Free Screenwriting
====================

Screenwriting on free software, I find, is best done in GNU Emacs using V. L. Simpson's screenwriter.el (or "screenwriter-mode").  This is available here:

http://www.nongnu.org/screenwriter/

You may also want to use my dot-emacs (.emacs) file, or at least the parts that pertain to screenwriter.el.  It adds defaults for screenwriter.el such as opening .scp and .screenplay files automatically in screenplay-writer, and providing alternate keybindings for the mode itself.

It's available from http://www.gitorious.org/emacs 

A convention I use myself is to name scenes that I write in screenwriter.el with the extension `.scp' and either complete streenplays or blocks of text to be processed as screenplays with the extension `.screenplay'.  This is obviously not a requirement, but `screenplay-tools' is written under these assumptions. 


2.3 Organization
================

I find that it is best to write each scene in its own file.

  bash$ cd ~/myScreenplayDir
  bash$ ls
  00.scp
  01.scp
  02.scp
  03.scp

In this way, scenes are easily re-arranged, shelved, archived, printed, etc.  In other words, take advantage of good old fashioned UNIX philosophy and keep it modular.  You pay $200 plus your computing freedom for that kind of flexibility in proprietary screenplay apps!


3 The Commands
**************

* screenplay-build::            Previewing your screenplay as a single document
* screenplay-title::            Generating a formatted title page
* screenplay-print::		Creating a printable document
* Generating a PDF::		Notes on generating a PDF
* screenplay-location::		Generating a report on locations and sets
* screenplay-character::	Generating a report on characters and casting
* screenplay-help::		Instant stdout help


3.1 screenplay-build
====================

For a quick preview of your screenplay as one big document, use

`screenplay-build ~/myScreenplayDir'

which simply concatenates anything appended with .scp into one big `preview.screenplay', indents it a little bit, and places that preview file in whatever directory you currently are in.

If you've configured your system with the dot-emacs options that Slackermedia recommends, then the preview.screenplay will open in screenwriter-mode in Emacs, and is obviously also readable (being plain text) in any other text editor you choose.

Note that this is intended as a quick, single-file preview of your screenplay only and does not contain page-numbers or scene numbers.


3.2 screenplay-title
===================================

To generate a properly formatted title page for your screenplay, use:

`screenplay-title'

which interactively creates and saves the file `title.page' with a capitalized and centered title, a correctly placed by-line, and your contact info. 


3.3 screenplay-print
====================

Once you feel you're ready to print your masterpiece on actual tree pulp and have generated a title.page, use

`screenplay-print [directory with all your .scp files in it]'

which will attempt to auto-detect the title and author from your title.page (although it allows for override), indent your script by 8 spaces to provide for a left margin, number scenes by occurence of SLUGLINES, add the title.page to the front of the document, and insert form feed characters to enforce page breaks. 

Finally, it will ask you to confirm printing via lpr.

If you are ready to print, then answer "y" and it will send the printable script and the title page to lpr (ie, your default line printer).

Otherwise, you'll be left with a print.screenplay, which you can review and print at your leisure.


3.2.1 screenplay-print options
------------------------------

`--no-print' will not bother you about whether or not you really want to print, and will instead default to saving a print-friendly version of your script to a file.



3.2.2 Notes about printing
--------------------------

This is all plain text, which is powerful, and it is usually easiest to just print straight from the command line in this case.  However, that does require you to have some printer set up to be your default line printer.  If you don't have that, you can open print.screenplay in Emacs and use its built-it postscript printing to do the job for you.

However, if you want the lpr printing to work, you should open up the CUPS admin panel in a web browser.  You reach this by opening a web browser to 127.0.0.1:631, and you will probably need the root password.

In the ADMINISTRATION panel of this page, you can MANAGE PRINTERS or add a new one if you haven't used a printer on the system yet.  Do that, and set a printer as an lpd:// printer, and set that as your default.

As an example, on Slackware GNU/Linux, I can go to CUPS and add a new printer.  I select the production office's Ricoh 1060 printer by way of its IP address:

   lpd://192.168.1.8

I choose its driver, which is conveniently already installed (but if it wasn't, I'd go to openprinting.org and find the ppd file).

I mark this printer as my default.

Now if I do:

    echo "test" | lpr 

I should get a page reading "test" moments later.  Success.


3.4 Generating a PDF
====================

If you want to save your screenplay to a PDF and email it to all your production staff and actors, you can do this a few different ways.

The easiest and most direct method is to download `text2pdf', an application written in C by Phil Smith, which takes plain text and creates a PDF.  You may download text2pdf here:

http://www.eprg.org/pdfcorner/text2pdf/

There is both the source code and a GNU MakeFile available for it.  To compile it, simply download both, place into a consolidated folder, and run make.  You can then move text2pdf to either ~/bin or a systemwide bin directory.

Once installed, you can do this:

     text2pdf print.screenplay > myGreatScreenplay.pdf

Which produces, obviously, myGreatScreenplay.pdf that will be viewable in any PDF viewer on basically any OS.

If, on the other hand, you prefer or want to use CUPS as a pdf "printer", then you can download the cups-pdf ppd from:

http://www.physik.uni-wuerzburg.de/~vrbehr/cups-pdf/

Install this, create a printer backend for it, and print to that.


This is more complex than using text2pdf but a good overview of it can be found here:

http://alien.slackbook.org/dokuwiki/doku.php?id=slackware:cups


3.5 screenplay-location
=======================

Once the screenplay is finalized, one of the first things you'll want are LOCATION reports and CHARACTER reports.

Probably you should have a preview.screenplay or a print.screenplay for these to work the way they are intended to work.  But there's nothing stopping you from running it on individual scene files, either.

To get a list of all locations in your screenplay, run this:

   screenplay-location [input file] [output file]

For example:

    screenplay-location print.screenplay location.report

This will interactively generate one of 4 types of location reports, or one that includes them all:

   1 List of all (unique) locations
   2 List of all INT. locations
   3 List of all EXT. locations
   4 List all locations in script order
   5 Generate all of the above in one report

By default, you end up with a printable report with appropriate headings to indicate what report it is.

If, however, you prefer to keep the reports digital, you can specify an output file with the `.org' extension, such as locations.org to produce a document without enforced page breaks or headers, formatted for use in org-mode's outline view.

Org-mode is a major mode for Emacs which excels at project management and is, along with screenwriter.el and screenplay-tools, all a movie production needs for its organizational and scheduling needs.  Learn more about it, and download it, from 

http://www.orgmode.org

screenplay-location is programmed to eliminate common transitions and other capitalized words from its list, but there is obviously a chance that something will get listed as a location when it is not.  You may need to review your location list for these quirks before sending it out to your entire production crew. 


3.6 screenplay-character
========================

Before you start casting, you will want a report of all the characters you've written into your movie so that you know what casting slots you must fill.

screenplay-character searches for all the CAPITALIZED character names with dialogue.  It will also, obviously, catch transitions, but it uses grep to eliminate those false positives before it finishes.  Also eliminated from the list are transitions, like CUT TO, FADE IN, FADE OUT, etc. -- but you should review the output just to make sure you're not doing a casting call for a character named DISSOLVE TO.

By default, this list is formatted for printing with appropriate headers and page breaks as needed.

If, however, you prefer to keep the reports digital, you can specify an output file with the `.org' extension, such as characters.org and you will end up with a document without enforced page breaks or headers, formatted for use in org-mode's outline view.


3.7 screenplay-help
===================

Run `screenplay-help' for a quick run-down on the toolset and syntax.  There are also man pages for each command, as well as this .info file.


3.8 Bugs
********

Too many to list.  This should all be re-implemented by someone who knows what they're doing.  I will attempt to make improvements but in the mean time, this is a working, functional time-saver that helps me get screenplays formatted, analyzed, and printed.  Good enough for now!

Email patches and stuff to me at klaatu at member dot fsf.org
