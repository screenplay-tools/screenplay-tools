#!/bin/bash
# check to see if we are running as root
ID=`id -u`
if [ "x$ID" != "x0" ]; then
	echo "/ / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / "
	echo "You must run this installer as root"
	echo "If you don't have root access, you can still read these man pages"
	echo "by running the command man ./man/screenplay-tools.1.gz"
	echo "and so on"
	echo "/ / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / "
        exit 1
fi
echo "installing man pages to /usr/share/man/man1/"
cp ./man/* /usr/share/man/man1/
